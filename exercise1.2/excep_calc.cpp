#include <iostream>
#include <string>

void noAccess(int a, int b, int result)
{
	if (a == 8200 || b == 8200 || result == 8200)
	{
		throw std::string("This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year");
	}

}

int add(int a, int b) {
	noAccess(a, b, a + b);
	return a + b;
}

int  multiply(int a, int b) {
	int sum = 0;

	for (int i = 0; i < b; i++)
	{
		sum = add(sum, a);
		noAccess(a, b, sum);
	}
	return sum;
}

int pow(int a, int b) {
	double exponent = 1.000;
	for (int i = 0; i < b; i++) {
		exponent = multiply(exponent, a);
		noAccess(a, b, exponent);
	};

	return exponent;
}

int main(void) {
	try {
		std::cout << pow(90, 2) << std::endl;
		std::cout << add(3, 200) << std::endl;
		std::cout << multiply(15, 20) << std::endl;
		std::cout << add(8000, 200) << std::endl;
	}
	catch (std::string error)
	{
		std::cout << error << std::endl;
	}
	system("pause");
}