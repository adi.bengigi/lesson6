#include "MathUtils.h"


/*
call pentagon: (5 * a^2 / 4) * cot(pi/5)
*/
double MathUtils::CalPentagonArea(double rib)
{
	double a = (5 * rib * rib) / 4;
	a = a * 1 / (tan(3.14 / 5));
	return a;
}


/*
call hexagon: (3 * a^2 / 2) * cot(pi/6)
*/
double MathUtils::CalHexagonArea(double rib)
{
	double a = (3 * rib * rib) / 2;
	a = a * 1 / (tan(3.14 / 6));
	return a;
}