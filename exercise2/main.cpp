#include <iostream>
#include "inputException.h"
#include "shape.h"
#include "circle.h"
#include "quadrilateral.h"
#include "rectangle.h"
#include "parallelogram.h"
#include <string>
#include "shapeException.h"
#include "MathUtils.h"
#include "Pentagon.h"
#include "Hexagon.h"

int main()
{
	std::string nam, col; double rad = 0, ang = 0, ang2 = 180; int height = 0, width = 0, rib = 0;
	Circle circ(col, nam, rad);
	quadrilateral quad(nam, col, width, height);
	rectangle rec(nam, col, width, height);
	parallelogram para(nam, col, width, height, ang, ang2);
	Pentagon pent(nam, col, rib);
	Hexagon hexa(nam, col, rib);

	Shape *ptrcirc = &circ;
	Shape *ptrquad = &quad;
	Shape *ptrrec = &rec;
	Shape *ptrpara = &para;
	Shape *ptrpent = &pent;
	Shape *ptrhexa = &hexa;


	std::cout << "Enter information for your objects" << std::endl;
	const char circle = 'c', quadrilateral = 'q', rectangle = 'r', parallelogram = 'p'; char shapetype;
	std::string strShapeType;
	char x = 'y';
	while (x != 'x') {
		std::cout << "\nwhich shape would you like to work with?.. \nc=circle, q = quadrilateral, r = rectangle, p = parallelogram, P = pentagon, h = hexagon" << std::endl;
		try //check if the input is more then one char
		{
			std::cin >> strShapeType;
			if (strShapeType.size() > 1)
			{
				strShapeType[0] = 'z';
				throw inputException();
			}
		}
		catch (std::exception e)
		{
			printf(e.what());
		}

		shapetype = strShapeType[0];

		try
		{

			switch (shapetype) {
			case 'c':
				std::cout << "enter color, name,  rad for circle" << std::endl;
				std::cin >> col >> nam >> rad;

				if (std::cin.fail())//check if the char is proper
				{
					std::cin.clear();
					throw inputException();
				}

				circ.setColor(col);
				circ.setName(nam);
				circ.setRad(rad);
				ptrcirc->draw();
				break;
			case 'q':
				std::cout << "enter name, color, height, width" << std::endl;
				std::cin >> nam >> col >> height >> width;
				if (std::cin.fail())//check if the char is proper
				{
					std::cin.clear();
					throw inputException();
				}
				quad.setName(nam);
				quad.setColor(col);
				quad.setHeight(height);
				quad.setWidth(width);
				ptrquad->draw();
				break;
			case 'r':
				std::cout << "enter name, color, height, width" << std::endl;
				std::cin >> nam >> col >> height >> width;
				if (std::cin.fail())//check if the char is proper
				{
					std::cin.clear();
					throw inputException();
				}
				rec.setName(nam);
				rec.setColor(col);
				rec.setHeight(height);
				rec.setWidth(width);
				ptrrec->draw();
				break;

			case 'p':
				std::cout << "enter name, color, height, width, 2 angles" << std::endl;
				std::cin >> nam >> col >> height >> width >> ang >> ang2;
				if (std::cin.fail())//check if the char is proper
				{
					std::cin.clear();
					throw inputException();
				}

				if (ang < 0 || ang2 < 0 || ang > 180 || ang2 > 180)//check if the angles is exception
				{
					throw shapeException();
				}

				para.setName(nam);
				para.setColor(col);
				para.setHeight(height);
				para.setWidth(width);
				para.setAngle(ang, ang2);
				ptrpara->draw();
				break;

			case 'P':
				std::cout << "enter name, color, rib" << std::endl;
				std::cin >> nam >> col >> rib;
				if (std::cin.fail())//check if the char is proper
				{
					std::cin.clear();
					throw inputException();
				}

				pent.setName(nam);
				pent.setColor(col);
				pent.settRib(rib);
				ptrpent->draw();
				break;

			case 'h':
				std::cout << "enter name, color, rib" << std::endl;
				std::cin >> nam >> col >> rib;
				if (std::cin.fail())//check if the char is proper
				{
					std::cin.clear();
					throw inputException();
				}
				hexa.setName(nam);
				hexa.setColor(col);
				hexa.setRib(rib);
				ptrhexa->draw();
				break;

			case 'z':
				std::cout << "\nWarning - Don't try to build more than one shape at once\n" << std::endl;
				break;
			default:
				std::cout << "you have entered an invalid letter, please re-enter" << std::endl;
				break;
			}
			std::cout << "would you like to add more object press any key if not press x" << std::endl;
			std::cin.clear();
			std::cin >> x;
		}
		catch (std::exception e)
		{
			printf(e.what());
		}
		catch (...)
		{
			printf("caught a bad exception. continuing as usual\n");
		}
	}

	system("pause");
	return 0;
}