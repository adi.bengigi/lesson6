#include "Pentagon.h"

Pentagon::Pentagon(std::string nam, std::string col, double rib) :Shape(nam, col)
{
	_rib = rib;
}

void Pentagon::draw()
{
	std::cout << std::endl << "Color is " << getColor() << std::endl << "Name is " << getName() << std::endl << "rib is " << gettRib() << std::endl << "area: " << MathUtils::CalPentagonArea(gettRib()) << std::endl;;
}

double Pentagon::gettRib()
{
	return _rib;
}

double Pentagon::settRib(double rib)
{
	if (0 > rib)
	{
		throw shapeException();
	}

	_rib = rib;
}