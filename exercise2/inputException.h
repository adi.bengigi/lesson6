#pragma once
#include <exception>

class inputException : public std::exception
{
	virtual const char* what() const
	{
		return "\nThis is a input exception!\n";
	}
};
