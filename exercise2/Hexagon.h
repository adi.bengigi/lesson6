#pragma once
#include "mathutils.h"
#include "shape.h"
#include "shapeexception.h"


class Hexagon : public Shape
{
public:
	Hexagon(std::string, std::string, double);
	~Hexagon() {};

	double getRib();
	void setRib(double rib);
	void draw();

private:
	double _rib;
};
