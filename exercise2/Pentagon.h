#pragma once
#include "MathUtils.h"
#include "shape.h"
#include "shapeexception.h"

class Pentagon : public Shape
{
public:
	Pentagon(std::string, std::string, double);
	~Pentagon() {};

	void draw();
	double gettRib();
	double settRib(double rib);

private:
	double _rib;
};
