#include "Hexagon.h"

Hexagon::Hexagon(std::string nam, std::string col, double rib) :Shape(nam, col)
{
	this->_rib = rib;
}


double Hexagon::getRib()
{
	return this->_rib;
}

void Hexagon::setRib(double rib)
{
	if (rib < 0)
	{
		throw shapeException();
	}
	this->_rib = rib;
}

void Hexagon::draw()
{
	std::cout << std::endl << "Color is " << getColor() << std::endl << "Name is " << getName() << std::endl << "rib is:" << getRib() << std::endl << "area:" << MathUtils::CalHexagonArea(getRib()) << std::endl;
}

