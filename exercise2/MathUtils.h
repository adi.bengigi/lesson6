#pragma once
#include <math.h>

class MathUtils
{

public:
	MathUtils() {};
	~MathUtils() {};

	static double CalPentagonArea(double rib);
	static double CalHexagonArea(double rib);
};

