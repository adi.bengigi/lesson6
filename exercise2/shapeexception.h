#pragma once
#include <exception>

class shapeException : public std::exception
{
	virtual const char* what() const
	{
		return "\nThis is a shape exception!\n";
	}
};
