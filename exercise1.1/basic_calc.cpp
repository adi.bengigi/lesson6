#include <iostream>

bool ok;

int add(int a, int b) {
	ok = noAccess(a, b, a + b);
	return a + b;
}

int  multiply(int a, int b) {
	int sum = 0;
	for (int i = 0; i < b; i++) {
		sum = add(sum, a);
		ok = noAccess(a, b, sum);
	};

	return sum;
}

int  pow(int a, int b) {
	int exponent = 1;
	for (int i = 0; i < b; i++) {
		exponent = multiply(exponent, a);
		ok = noAccess(a, b, exponent);
	};
	return exponent;
}

/*
checking if the values = 8200 and save it in 'ok' whitch is boolean
*/
bool noAccess(int a, int b, int result)
{
	if (a == 8200 || b == 8200 || result == 8200)
	{
		ok = false;
	}
	else
	{
		ok = true;
	}
	return ok;
}

/*if was 8200 it will return the error meseg
else continue
*/
void check(int result)
{
	if (ok == true)
	{
		std::cout << result << std::endl;
	}
	else
	{
		std::cout << "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year\n" << std::endl;
	}
}

int main(void) {
	int result = 0;

	result = pow(5, 5);
	check(result);

	result = multiply(8200, 3);
	check(result);

	system("pause");
}